package pl.codementors.primework;

import java.io.File;
import java.util.Scanner;

public class PrimeMain {

    public static void main(String[] args) {

        startPrimeWorker();
    }


    static boolean isPrime(int number) {

        if (number == 2)
            return true;
        if (number < 2 || number % 2 == 0)
            return false;
        for (int i = 3; i * i <= number; i += 2)
            if (number % i == 0)
                return false;
        return true;
    }

    private static void startPrimeWorker() {
        ResultResource resource = new ResultResource(new File("test"));
        TaskResource taskResource = new TaskResource();
        PrimeWorker numberprime1 = new PrimeWorker(taskResource, resource);
        PrimeWorker numberprime2 = new PrimeWorker(taskResource, resource);
        PrimeWorker numberprime3 = new PrimeWorker(taskResource, resource);
        PrimeWorker numberprime4 = new PrimeWorker(taskResource, resource);
        PrimeWorker numberprime5 = new PrimeWorker(taskResource, resource);


        new Thread(numberprime1).start();
        new Thread(numberprime2).start();
        new Thread(numberprime3).start();
        new Thread(numberprime4).start();
        new Thread(numberprime5).start();

        Scanner scanner = new Scanner(System.in);
        while (true) {
            int i = scanner.nextInt();
            if (i==0){
                break;
            }
            taskResource.put(i);
        }
        numberprime1.stop();
        numberprime2.stop();
        numberprime3.stop();
        numberprime4.stop();
        numberprime5.stop();
    }
}
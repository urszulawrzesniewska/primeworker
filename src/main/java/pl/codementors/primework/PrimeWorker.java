package pl.codementors.primework;

public class PrimeWorker implements Runnable {

    private boolean running = true;

    private TaskResource taskResource;

    public void stop() {
        running = false;
    }

    public PrimeWorker(TaskResource taskResource, ResultResource result) {
        this.taskResource = taskResource;
        this.result = result;
    }

    private ResultResource result;


    @Override
    public void run() {
        while (running) {
            try {
                int number = taskResource.take();
                result.save(number + " " + PrimeMain.isPrime(number));
            } catch (InterruptedException ex) {
                System.err.println(ex);
            }

        }
    }
}
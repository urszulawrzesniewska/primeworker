package pl.codementors.primework;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ResultResource {

    private File outputFile; //pole typu file, nazwa pliku nieznana

    public ResultResource(File outputFile) { //to jest konstruktor i opis tegoz
        this.outputFile = outputFile;
    }

    public synchronized void save(String line) { //synchronized pozwoli by zachowywalo sie tak by tylko 1 watek jednoczesnie korzystal z metody
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(outputFile, true)); //true pozwoli dopisywac wyniki, a nie nadpisywac
           bw.write(line);
        } catch (IOException ex) {
            System.err.println(ex);
        } finally {
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ex) {
                    System.err.println(ex);
                }
            }
        }
    }
}
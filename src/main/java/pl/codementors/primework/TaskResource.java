package pl.codementors.primework;

public class TaskResource {

    private int task;

    public synchronized void put(int i) {
        this.task = i;
        notifyAll();
    }

    public synchronized int take() throws InterruptedException {
        while (task ==0) {
            this.wait();//Thread in wait status is outside the critical section.
        }
        int ret = task;
        task = 0;
        return ret;
    }
}
